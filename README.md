# SOC_coding_tutorial_aMRI

### Welcome

In this repository, you can find the material and data for the School Of Cognition Coding Tutorial on anatomical MRI.  
If you want to learn more about git and how to use it, I can recommend this [simple guide](https://rogerdudler.github.io/git-guide/).


### Cloning the repository
Please clone this repository to your jupyter-hub.

1. Go to the jupyter hub and open a terminal at the right upper corner: `New` -> `Terminal`

2. Type:`git clone https://gitlab.gwdg.de/frauke.beyer/soc_coding_tutorial_aMRI.git`

3. You have copied the notebooks and data into your jupyter hub. 