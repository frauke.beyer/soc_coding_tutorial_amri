Node: apply_inv2_mask (fsl)
===========================


 Hierarchy : afMRI_coreg.apply_inv2_mask
 Exec ID : apply_inv2_mask


Original Inputs
---------------


* args : <undefined>
* environ : {'FSLOUTPUTTYPE': 'NIFTI_GZ'}
* in_file : /home/jovyan/soc_coding_tutorial_aMRI/anat_data/sub-010088_ses-01_acq-mp2rage_T1w.nii.gz
* in_file2 : <undefined>
* mask_file : /home/jovyan/soc_coding_tutorial_aMRI/anat_data/wd/afMRI_coreg/bet_inv2/sub-010088_ses-01_inv-2_mp2rage_brain_mask.nii.gz
* op_string : <undefined>
* out_data_type : <undefined>
* out_file : <undefined>
* output_type : NIFTI_GZ
* suffix : <undefined>


Execution Inputs
----------------


* args : <undefined>
* environ : {'FSLOUTPUTTYPE': 'NIFTI_GZ'}
* in_file : /home/jovyan/soc_coding_tutorial_aMRI/anat_data/sub-010088_ses-01_acq-mp2rage_T1w.nii.gz
* in_file2 : <undefined>
* mask_file : /home/jovyan/soc_coding_tutorial_aMRI/anat_data/wd/afMRI_coreg/bet_inv2/sub-010088_ses-01_inv-2_mp2rage_brain_mask.nii.gz
* op_string : <undefined>
* out_data_type : <undefined>
* out_file : <undefined>
* output_type : NIFTI_GZ
* suffix : <undefined>


Execution Outputs
-----------------


* out_file : /home/jovyan/soc_coding_tutorial_aMRI/anat_data/wd/afMRI_coreg/apply_inv2_mask/sub-010088_ses-01_acq-mp2rage_T1w_maths.nii.gz


Runtime info
------------


* cmdline : fslmaths /home/jovyan/soc_coding_tutorial_aMRI/anat_data/sub-010088_ses-01_acq-mp2rage_T1w.nii.gz -mas /home/jovyan/soc_coding_tutorial_aMRI/anat_data/wd/afMRI_coreg/bet_inv2/sub-010088_ses-01_inv-2_mp2rage_brain_mask.nii.gz /home/jovyan/soc_coding_tutorial_aMRI/anat_data/wd/afMRI_coreg/apply_inv2_mask/sub-010088_ses-01_acq-mp2rage_T1w_maths.nii.gz
* duration : 4.567826
* hostname : 126ba66e42b5
* prev_wd : /home/jovyan/soc_coding_tutorial_aMRI
* working_dir : /home/jovyan/soc_coding_tutorial_aMRI/anat_data/wd/afMRI_coreg/apply_inv2_mask


Terminal output
~~~~~~~~~~~~~~~


 


Terminal - standard output
~~~~~~~~~~~~~~~~~~~~~~~~~~


 


Terminal - standard error
~~~~~~~~~~~~~~~~~~~~~~~~~


 


Environment
~~~~~~~~~~~


* CLICOLOR : 1
* CONDA_DIR : /opt/conda
* CONDA_VERSION : 4.7.12
* DEBIAN_FRONTEND : noninteractive
* FSLDIR : /home/jovyan/fsl/
* FSLOUTPUTTYPE : NIFTI_GZ
* GIT_PAGER : cat
* HOME : /home/jovyan
* HOSTNAME : 126ba66e42b5
* JPY_API_TOKEN : bb7ed9ea0cb946168ae06c36f32fe963
* JPY_PARENT_PID : 6
* JULIA_DEPOT_PATH : /opt/julia
* JULIA_PKGDIR : /opt/julia
* JULIA_VERSION : 1.2.0
* JUPYTERHUB_ACTIVITY_URL : http://jupyterhub:8080/hub/api/users/0512920/activity
* JUPYTERHUB_ADMIN_ACCESS : 1
* JUPYTERHUB_API_TOKEN : bb7ed9ea0cb946168ae06c36f32fe963
* JUPYTERHUB_API_URL : http://jupyterhub:8080/hub/api
* JUPYTERHUB_BASE_URL : /
* JUPYTERHUB_CLIENT_ID : jupyterhub-user-0512920
* JUPYTERHUB_HOST : 
* JUPYTERHUB_OAUTH_CALLBACK_URL : /user/0512920/oauth_callback
* JUPYTERHUB_SERVER_NAME : 
* JUPYTERHUB_SERVICE_PREFIX : /user/0512920/
* JUPYTERHUB_USER : 0512920
* KERNEL_LAUNCH_TIMEOUT : 40
* KMP_INIT_AT_FORK : FALSE
* LANG : en_US.UTF-8
* LANGUAGE : en_US.UTF-8
* LC_ALL : en_US.UTF-8
* MEM_LIMIT : 10737418240
* MINICONDA_MD5 : 1c945f2b3335c7b2b15130b1b2dc5cf4
* MINICONDA_VERSION : 4.7.10
* MPLBACKEND : module://ipykernel.pylab.backend_inline
* NB_GID : 100
* NB_UID : 1000
* NB_USER : jovyan
* PAGER : cat
* PATH : /opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/home/jovyan/fsl/bin
* PIP_DISABLE_PIP_VERSION_CHECK : 1
* PWD : /home/jovyan
* SHELL : /bin/bash
* SHLVL : 0
* TERM : xterm-color
* XDG_CACHE_HOME : /home/jovyan/.cache/

